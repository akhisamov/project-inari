#include "Editor.h"

#include "imgui/imgui_impl_glfw.h"
#include "imgui/imgui_impl_opengl3.h"

Editor::Editor()
    : m_log_scroll_to_bottom(false)
    , m_log_count(0)
    , m_log_wrap(false)
{
}

void Editor::configure(GLFWwindow *window)
{
    m_window = window;
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    auto &io = ImGui::GetIO();
    io.IniFilename = nullptr;
    (void)io;

    ImGui::StyleColorsDark();

    ImGui_ImplGlfw_InitForOpenGL(m_window, true);
    ImGui_ImplOpenGL3_Init();

    ImGui::GetStyle().WindowRounding = 0.0f;
    ImGui::GetStyle().Colors[ImGuiCol_TitleBgActive] = ImVec4(0.94, 0.36, 0.11, 1);
    ImGui::GetStyle().Colors[ImGuiCol_TitleBg] = ImGui::GetStyle().Colors[ImGuiCol_TitleBgActive];
    ImGui::GetStyle().Colors[ImGuiCol_TitleBgCollapsed] = ImGui::GetStyle().Colors[ImGuiCol_TitleBgActive];
}

void Editor::draw()
{
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();

    if (m_window)
    {
        auto width = 0;
        auto height = 0;
        glfwGetFramebufferSize(m_window, &width, &height);

        auto mainMenuBarHeight = drawMainMenuBar();

        const auto window_height = height - mainMenuBarHeight;

        auto size = ImVec2(width * 0.6, window_height * 0.2);
        drawLogger(size, ImVec2(width * 0.2, height - size.y));

        size = ImVec2(width * 0.2, window_height * 0.7);
        drawHierarchy(size, ImVec2(width - size.x, mainMenuBarHeight));

        size = ImVec2(width * 0.2, window_height * 0.3);
        drawProperties(size, ImVec2(width - size.x, height - size.y));

        size = ImVec2(width * 0.6, window_height * 0.8);
        drawPlayground(size, ImVec2(width * 0.2, mainMenuBarHeight));

        size = ImVec2(width * 0.2, height - mainMenuBarHeight);
        drawFileSystem(size, ImVec2(0, height - size.y));
    }

    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}

void Editor::clean()
{
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();
}

void Editor::addLog(const std::string &message)
{
    m_log_buffer.append(message.c_str());
    m_log_scroll_to_bottom = true;
    m_log_count++;
}

int Editor::drawMainMenuBar()
{
    auto height = 0;
    if (ImGui::BeginMainMenuBar())
    {
        height = ImGui::GetWindowHeight();
        if (ImGui::BeginMenu("File"))
        {
            ImGui::MenuItem("Test");
            ImGui::Separator();
            ImGui::MenuItem("Exit");
            ImGui::EndMenu();
        }
        ImGui::EndMainMenuBar();
    }
    return height;
}

void Editor::drawLogger(const ImVec2 &size, const ImVec2 &pos)
{
    ImGui::SetNextWindowSize(size);
    ImGui::PushStyleVar(ImGuiStyleVar_WindowTitleAlign, ImVec2(0.5f, 0.5f));

    auto title = "Logger | " + std::to_string(m_log_count) + " messages";
    ImGui::Begin(title.c_str(), nullptr,
                 ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove);
    ImGui::SetWindowPos(pos);

    if (ImGui::Button("Clear"))
    {
        m_log_buffer.clear();
        m_log_count = 0;
    }
    ImGui::SameLine();
    ImGui::Checkbox("Word Wrap", &m_log_wrap);
    ImGui::Separator();

    ImGui::BeginChild("scrolling");
    ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(0, 1));

    if (m_log_wrap)
    {
        ImGui::TextWrapped(m_log_buffer.begin());
    }
    else
    {
        ImGui::TextUnformatted(m_log_buffer.begin());
    }
    if (m_log_scroll_to_bottom)
    {
        ImGui::SetScrollHere(1.0f);
    }
    m_log_scroll_to_bottom = false;

    ImGui::PopStyleVar();
    ImGui::EndChild();
    ImGui::End();

    ImGui::PopStyleVar();
}

void Editor::drawHierarchy(const ImVec2 &size, const ImVec2 &pos)
{
    ImGui::SetNextWindowSize(size);
    ImGui::Begin("Hierarchy", nullptr,
                 ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove);
    ImGui::SetWindowPos(pos);
    ImGui::End();
}

void Editor::drawProperties(const ImVec2 &size, const ImVec2 &pos)
{
    ImGui::SetNextWindowSize(size);
    ImGui::Begin("Properties", nullptr,
                 ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove);
    ImGui::SetWindowPos(pos);
    ImGui::End();
}

void Editor::drawPlayground(const ImVec2 &size, const ImVec2 &pos)
{
    ImGui::SetNextWindowSize(size);
    ImGui::Begin("Playground", nullptr,
                 ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize |
                     ImGuiWindowFlags_NoMove);
    ImGui::SetWindowPos(pos);
    ImGui::Text("Playground");
    ImGui::End();
}

void Editor::drawFileSystem(const ImVec2 &size, const ImVec2 &pos)
{
    ImGui::SetNextWindowSize(size);
    ImGui::PushStyleVar(ImGuiStyleVar_WindowTitleAlign, ImVec2(1.0f, 0.5f));
    ImGui::Begin("FileSystem", nullptr,
                 ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove);
    ImGui::SetWindowPos(pos);
    ImGui::End();
    ImGui::PopStyleVar();
}