#include <iostream>

#include "InariEngine.h"
#include "hands/Configuration.h"
#include "hands/FileSystem.h"
#include "hands/Logger.hpp"

int main(int argc, char **argv)
{
    Logger logger;
    Configuration config(FileSystem::instance().getBinPath("config.ini"),
                         "[Video]\nWidth=1280\nHeight=720\nFullscreen=0");
    try
    {
        InariEngine Fox(config, logger);
        Fox.run();
    }
    catch (const std::runtime_error &e)
    {
        logger.error(e.what());

#ifdef _DEBUG
        std::cerr << "\n\nPress any key!" << std::endl;
        std::cin.get();
#endif

        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
