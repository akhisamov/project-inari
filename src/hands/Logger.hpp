#pragma once

#include "FileSystem.h"

#include <ctime>
#include <functional>
#include <iostream>
#include <locale>
#include <sstream>
#include <string>
#include <vector>

#include <boost/date_time.hpp>

class Logger
{
  public:
    Logger()
        : m_log(10, '-')
        , m_file("")
        , m_save_always(false)
        , m_log_callback([](const std::string &) {
        })
    {
        m_log += '\n';
    }
    Logger(const std::string &file, bool save_always = false)
        : m_log(FileSystem::instance().readFile(file))
        , m_file(file)
        , m_save_always(save_always)
        , m_log_callback([](const std::string &) {
        })
    {
        m_log += std::string(10, '-') + '\n';
    }

    const std::string &getLog()
    {
        return m_log;
    }

    void saveToFile(const std::string &file = "")
    {
        m_file = file.empty() ? m_file : file;
        if (!m_file.empty())
        {
            FileSystem::instance().writeFile(m_file, m_log, std::ofstream::out | std::ofstream::trunc);
        }
        else
        {
            error("Can't save logs because path to the log file is empty");
        }
    }
    void saveAlways(bool toggle)
    {
        m_save_always = toggle;
    }

    void setLogCallback(std::function<void(const std::string &)> log_callback)
    {
        m_log_callback = log_callback;
    }

    template <typename... T>
    void error(const std::string &format, const T &... arguments)
    {
        log(Mode::ERR, format, arguments...);
    }
    template <typename... T>
    void warning(const std::string &format, const T &... arguments)
    {
        log(Mode::WARNING, format, arguments...);
    }
    template <typename... T>
    void comment(const std::string &format, const T &... arguments)
    {
        log(Mode::COMMENT, format, arguments...);
    }
    template <typename... T>
    void debug(const std::string &format, const T &... arguments)
    {
#ifdef _DEBUG
        log(Mode::DEBUG, format, arguments...);
#endif
    }

  private:
    std::string m_log;
    std::string m_file;
    bool m_save_always;
    std::function<void(const std::string &)> m_log_callback;

    enum class Mode
    {
        ERR = 0,
        WARNING = 1,
        COMMENT = 2,
        DEBUG = 3
    };

    template <typename... T>
    void log(Mode mode, const std::string &format, const T &... arguments)
    {
        size_t len = std::snprintf(nullptr, 0, format.c_str(), arguments...) + 1;
        std::vector<char> message(len);
        std::snprintf(&message[0], len, format.c_str(), arguments...);

        std::stringstream datetime;
        datetime.imbue(std::locale(std::cout.getloc(), new boost::posix_time::time_facet("%D %T")));
        datetime << boost::posix_time::second_clock::local_time();

        std::string mode_str;
        switch (mode)
        {
        case Mode::ERR:
            mode_str = "ERROR";
            break;
        case Mode::WARNING:
            mode_str = "WARNING";
            break;
        case Mode::COMMENT:
            mode_str = "COMMENT";
            break;
        case Mode::DEBUG:
            mode_str = "DEBUG";
            break;
        }

        len = std::snprintf(NULL, 0, "%s [%s]: %s\n", datetime.str().c_str(), mode_str.c_str(), &message[0]);
        std::vector<char> log(len + 1);
        std::snprintf(&log[0], len + 1, "%s [%s]: %s\n", datetime.str().c_str(), mode_str.c_str(), &message[0]);
        m_log += &log[0];

#ifdef _DEBUG
        std::cout << &log[0] << std::flush;
#endif
        if (mode != Mode::DEBUG)
        {
            m_log_callback(&log[0]);
        }
        if (m_save_always)
        {
            saveToFile();
        }
    }
};